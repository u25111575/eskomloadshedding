clean:
	del output\*.txt

sample:
	py -3 main.py input\small\users.xml input\small\badges.xml -o output\

vis:
	py -3 main.py
	
run:
	py -3 main.py input\big\users.xml input\big\badges.xml -o output\
	
mr1:
	py -3 main.py 1 'input\StatsSA\Population (2002 - 2018).csv' 'input\SAPS\SAPS (2005 - 2018).csv' -o output\

mr2:
	py -3 main.py 2 'input\SAPS\SAPS (2005 - 2018).csv' -o output\

mr2s:
	py -3 main.py 2 'input\SAPS\SAPS_smaller.csv' -o output\

mr3:
	py -3 main.py 3 'input\IEC\National Elections (2014).csv' 'input\SAPS\SAPS (2005 - 2018).csv' 'input\StatsSA\Population (2002 - 2018).csv' -o output\

mr3b:
	py -3 main.py 3 'input\IEC\National Elections (2014).csv' 'input\IEC\National Elections (2009).csv' 'input\results\mr1.csv' -o output\

mr4:
	py -3 main.py 4 'input\IEC\Local Elections (2006).csv' 'input\IEC\Local Elections (2011).csv' 'input\IEC\Local Elections (2016).csv' -o output\

mr4a:
	py -3 main.py 4a 'input\IEC\National Elections (2014).csv' -o output\

mr4b:
	py -3 main.py 4b 'input\Results\mr4party.csv' -o output\

mr4c:
	py -3 main.py 4c 'input\SAPS\SAPS_smaller.csv' 'input\Other\Sub Places (2001).csv'  -o output\

mr4d:
	py -3 main.py 4d 'input\Results\mr4electionchange.csv' 'input\Results\saps_station_municipality_code2.csv'  -o output\

mr5:
	py -3 main.py 5 'input\SAPS\SAPS (2005 - 2018).csv' 'input\Other\Sub Places (2001).csv' 'input\Other\Municipalities (2007).csv' 'input\StatsSA\Community Survey Houshold (2016).csv' -o output\

mr5a:
	py -3 main.py 5a 'input\StatsSA\Community Survey Houshold (2016).csv' -o output\

mr5b:
	py -3 main.py 5b 'input\Results\mr5poverty.csv' 'input\Results\saps_station_municipality_code2.csv' -o output\

mr5x:
	py -3 main.py 5x 'input\SAPS\SAPS (2005 - 2018).csv' 'input\Other\Main Places (2001).csv'  -o output\

mr5xx:
	py -3 main.py 5xx 'input\Results\saps_station_municipality2.csv' 'input\Other\Municipalities (2007).csv'  -o output\

mr5s:
	py -3 main.py 5 'input\StatsSA\Community Survey Houshold (2016).csv' 'input\SAPS\SAPS_smaller.csv' 'input\Other\Municipalities (2007).csv' -o output\

mr6:
	py -3 main.py 6 'input\SAPS\SAPS (2005 - 2018).csv' 'input\StatsSA\Tourism (2011).csv' 'input\Other\Cities (2017).csv' -o output\
